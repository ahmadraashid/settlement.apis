﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Settlement.Models
{
    public class ActionResponse
    {
        public int ReturnedId { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }

    public class District
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Tehsil
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class GirdawarCircle
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Moza
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Girdawar
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ChangeGirdawarCircle
    {
        [Required]
        public int GirdawarId { get; set; }
        [Required]
        public int GirdawarCircleId { get; set; }
        [Required]
        public DateTime DateEntered { get; set; }
    }

    public class GirdawarView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GirdawarCircle { get; set; }
        public DateTime SinceDated { get; set; }
    }

    public class NewGirdawar
    {
        [Required]
        public int GirdawarCircleId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Dated { get; set; }
    }

    public class PatwariView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Moza { get; set; }
        public DateTime SinceDated { get; set; }
    }

    public class NewPatwari
    {
        [Required]
        public int MozaId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Dated { get; set; }
    }

    public class AssignGirdawar
    {
        [Required]
        public int GirdawarId { get; set; }
        [Required]
        public int CircleId { get; set; }
        [Required]
        public DateTime DateEntered { get; set; }
    }

    public class ReleaseGirdawar
    {
        [Required]
        public int GirdawarId { get; set; }
        [Required]
        public int CircleId { get; set; }
    }

    public class AssignSO
    {
        [Required]
        public int SOId { get; set; }
        [Required]
        public int DistrictId { get; set; }
    }

    public class AssignPatwari
    {
        [Required]
        public int PatwariId { get; set; }
        [Required]
        public int MozaId { get; set; }
        [Required]
        public DateTime DateEntered { get; set; }
    }

    public class ReleasePatwari
    {
        [Required]
        public int PatwariId { get; set; }
        [Required]
        public int MozaId { get; set; }
    }

    public class NewSettlementOfficer
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int DistrictId { get; set; }
    }

    public class SettlementOfficer
    {
        public int Id { get; set; }
        public string District { get; set; }
        public DateTime DatedEntered { get; set; }
        public DateTime DatedLeft { get; set; }
        public CurrentStatus Status { get; set; }
    }

    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ManageRoles
    {
        public string UserName { get; set; }
        public List<string> Roles { get; set; }
    }

    public class UserView
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string District { get; set; }
        public int DistrictId { get; set; }
        public List<string> Roles { get; set; }
    }

    public class UserList
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
    }

    public class AuthenticateModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }

    public class NewUser
    {
        public int DistrictId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public List<string> Roles { get; set; }
    }

    public class MonthlyProgress
    {
        [Required]
        public int Year { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int MozaId { get; set; }
        public int KhatooniWriting { get; set; }
        public int ShajraNasbWriting { get; set; }
        public int AksPatangi { get; set; }
        public int KhassraMeasurement { get; set; }
        public int FieldBook { get; set; }
        public int MisleHaqiyat { get; set; }
        public int Mausaavis { get; set; }
        public int ShajraKishtwaar { get; set; }
        public int SOId { get; set; }
        public string Description { get; set; }
    }

    public class UpdateProgressStatus
    {
        public int DistrictId { get; set; }
        public List<int> Ids { get; set; }
    }

    public class TokenView
    {
        public string Id { get; set; }
        public string Token { get; set; }
        public string FullName { get; set; }
        public string District { get; set; }
        public List<string> Roles { get; set; }
    }

    public class MonthlyProgressView
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int MozaId { get; set; }
        public string Moza { get; set; }
        public int KhatooniWriting { get; set; }
        public int ShajraNasbWriting { get; set; }
        public int AksPatangi { get; set; }
        public int KhassraMeasurement { get; set; }
        public int FieldBook { get; set; }
        public int MisleHaqiyat { get; set; }
        public int Mausaavis { get; set; }
        public int ShajraKishtwaar { get; set; }
        public string SettlementOfficer { get; set; }
        public ApprovalStatus IsApproved { get; set; }
        public DateTime Dated { get; set; }
        public string Description { get; set; }
    }

    public class UpdateMonthlyProgress
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int MozaId { get; set; }
        public int KhatooniWriting { get; set; }
        public int ShajraNasbWriting { get; set; }
        public int AksPatangi { get; set; }
        public int KhassraMeasurement { get; set; }
        public int FieldBook { get; set; }
        public int MisleHaqiyat { get; set; }
        public int Mausaavis { get; set; }
        public int ShajraKishtwaar { get; set; }
        public string Description { get; set; }
    }

    public class MonitoringVisits
    {
        public int Id { get; set; }
        public string Visitor { get; set; }
        public string Moza { get; set; }
        public int ProgressId { get; set; }
        public string Remarks { get; set; }
        public bool IsOkay { get; set; }
        public string Dated { get; set; }
    }

    public class NewMonitoringVisit
    {
        [Required]
        public string VisitorId { get; set; }
        [Required]
        public int MozaId { get; set; }
        [Required]
        public int RoleId { get; set; }
        [Required]
        public string Remarks { get; set; }
        [Required]
        public bool IsOkay { get; set; }
        public DateTime Dated { get; set; }
    }

    public class VisitsFilter
    {
        [Required]
        public string StartDate { get; set; }
        [Required]
        public string EndDate { get; set; }
    }
}
