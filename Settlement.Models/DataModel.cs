﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Settlement.Models
{
    public enum CurrentStatus
    {
        Active = 1,
        NotActive = 0
    }

    public enum ApprovalStatus
    {
        Approved = 1,
        NotApproved = 0,
        Rejected = 2
    }

    public class EFDistrict
    {
        public EFDistrict()
        {
            this.Tehsils = new List<EFTehsil>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public List<EFTehsil> Tehsils { get; set; }
    }

    public class EFTehsil
    {
        public EFTehsil()
        {
            this.GirdawarCircles = new List<EFGirdawarCircle>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public EFDistrict District { get; set; }

        public List<EFGirdawarCircle> GirdawarCircles { get; set; }
    }

    public class EFGirdawarCircle
    {
        public EFGirdawarCircle()
        {
            this.Mozas = new List<EFMoza>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public EFTehsil Tehsil { get; set; }

        public List<EFMoza> Mozas { get; set; }
    }

    public class EFMoza
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public EFGirdawarCircle GirdawarCircle { get; set; }
        public int TotalKhatas { get; set; }
        public int TotalKhatoonis { get; set; }
        public int TotalKhasraJaat { get; set; }
        public int TotalMutations { get; set; }
        public int PreWrittenKhatoonis { get; set; }
        public int PreMeasurements { get; set; }
        public int FieldBooksVerified { get; set; }
    }

    public class EFTehsilDar
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }

    public class EFTehsilDari
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateEntered { get; set; }
        public DateTime DateLeft { get; set; }
        [ForeignKey("Tehsil")]
        public int TehsilId { get; set; }
        [ForeignKey("TehsilDar")]
        public int TehsilDarId { get; set; }
        public EFTehsilDar TehsilDar { get; set; }
        public CurrentStatus Status { get; set; }
    }

    public class EFGirdawar
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }

    public class EFCircleGirdawari
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateEntered { get; set; }
        public DateTime DateLeft { get; set; }
        [ForeignKey("Girdawar")]
        public int GirdawarId { get; set; }
        public EFGirdawar Girdawar { get; set; }
        [ForeignKey("GirdawarCircle")]
        public int GirdawarCircleId { get; set; }
        public EFGirdawarCircle GirdawarCircle { get; set; }
        public CurrentStatus Status { get; set; }
    }

    public class EFPatwari
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

    }

    public class EFMozaPatwari
    {
        [Required]
        public int Id { get; set; }
        public DateTime DateEntered { get; set; }
        public DateTime DateLeft { get; set; }
        [ForeignKey("Moza")]
        public int MozaId { get; set; }
        public EFMoza Moza { get; set; }
        [ForeignKey("Patwari")]
        public int PatwariId { get; set; }
        public EFPatwari Patwari { get; set; }
        public CurrentStatus Status { get; set; }
    }

    public class EFSettlementOfficer
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateEntered { get; set; }
        public DateTime DateLeft { get; set; }
        [ForeignKey("District")]
        public int DistrictId { get; set; }
        public EFDistrict District { get; set; }
        public CurrentStatus Status { get; set; }
        public string UserName { get; set; }
    }

    public class EFMonthlyProgress
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        public int Month { get; set; }
        public int DistrictId { get; set; }
        [Required]
        public EFMoza Moza { get; set; }
        public int KhatooniWriting { get; set; }
        public int ShajraNasbWriting { get; set; }
        public int AksPatangi { get; set; }
        public int KhassraMeasurement { get; set; }
        public int FieldBook { get; set; }
        public int MisleHaqiyat { get; set; }
        public int Mausaavis { get; set; }
        public int ShajraKishtwaar { get; set; }
        public string Description { get; set; }
        public DateTime Dated { get; set; }
        public EFSettlementOfficer SO { get; set; }
        [Required]
        public ApprovalStatus Status { get; set; }
    }

    public class EFUser
    {
        public EFUser()
        {
            this.Roles = new List<EFUserRoles>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public string FullName { get; set; }
        [Required]
        public CurrentStatus Status { get; set; }
        [Required]
        public EFDistrict District { get; set; }
        public List<EFUserRoles> Roles { get; set; }
    }

    public class EFRole
    {
        [Key]
        public int Id { get; set; }
        public string Role { get; set; }
    }

    public class EFUserRoles
    {
        [ForeignKey("User")]
        public int UserId { get; set; }
        [ForeignKey("Role")]
        public int RoleId { get; set; }
        public EFUser User { get; set; }
        public EFRole Role { get; set; }
    }

    public class EFMonitoringVisits
    {
        public int Id { get; set; }
        public EFUser User { get; set; }
        public EFMoza Moza { get; set; }
        public EFRole Role { get; set; }
        public string Remarks { get; set; }
        public bool IsOkay { get; set; }
        public DateTime Dated { get; set; }
    }

    public class EFSOVisits
    {
        public int Id { get; set; }
        public EFSettlementOfficer SO { get; set; }
        public EFMoza Moza { get; set; }
        public string Remarks { get; set; }
        public bool IsOkay { get; set; }
        public DateTime Dated { get; set; }
    }

    public class EFTehsildarVisits
    {
        public int Id { get; set; }
        public EFTehsilDar TehsilDar { get; set; }
        public EFMoza Moza { get; set; }
        public string Remarks { get; set; }
        public bool IsOkay { get; set; }
        public DateTime Dated { get; set; }
    }

    public class EFGirdawarVisits
    {
        public int Id { get; set; }
        public EFGirdawar Girdawar { get; set; }
        public EFMoza Moza { get; set; }
        public string Remarks { get; set; }
        public bool IsOkay { get; set; }
        public DateTime Dated { get; set; }
    }


}
