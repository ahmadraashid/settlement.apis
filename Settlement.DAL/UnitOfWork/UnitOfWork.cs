﻿using Settlement.DAL.Repository;
using Settlement.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Settlement.DAL.UnitOfWork
{
    public class UnitOfWork : IDisposable
    {
        private SettlementDbContext context = null;
        private GenericRepository<EFDistrict> districtRepository;
        private GenericRepository<EFTehsil> tehsilRepository;
        private GenericRepository<EFGirdawarCircle> girdawarCircleRepository;
        private GenericRepository<EFMoza> mozaRepository;
        private GenericRepository<EFTehsilDar> tehsilDarRepository;
        private GenericRepository<EFGirdawar> girdawarRepository;
        private GenericRepository<EFCircleGirdawari> circleGirdawariRepository;
        private GenericRepository<EFPatwari> patwariRepository;
        private GenericRepository<EFMozaPatwari> mozaPatwariRepository;
        private GenericRepository<EFSettlementOfficer> settlementOfficerRepository;
        private GenericRepository<EFMonthlyProgress> progressRepository;
        private GenericRepository<EFUser> userRepository;
        private GenericRepository<EFRole> roleRepository;
        private GenericRepository<EFUserRoles> userRolesRepository;
        private GenericRepository<EFSOVisits> soVisitsRepository;
        private GenericRepository<EFTehsildarVisits> tehsildarVisitsRepository;
        private GenericRepository<EFGirdawarVisits> girdawarVisitsRepository;
        private GenericRepository<EFMonitoringVisits> userVisitsRepository;

        public UnitOfWork(SettlementDbContext dbContext)
        {
            context = dbContext;
        }

        public GenericRepository<EFDistrict> DistrictRepository
        {
            get
            {
                if (this.districtRepository == null)
                    this.districtRepository = new GenericRepository<EFDistrict>(context);
                return this.districtRepository;
            }
        }

        public GenericRepository<EFTehsil> TehsilRepository
        {
            get
            {
                if (this.tehsilRepository == null)
                    this.tehsilRepository = new GenericRepository<EFTehsil>(context);
                return this.tehsilRepository;
            }
        }

        public GenericRepository<EFGirdawarCircle> GirdawarCircleRepository
        {
            get
            {
                if (this.girdawarCircleRepository == null)
                    this.girdawarCircleRepository = new GenericRepository<EFGirdawarCircle>(context);
                return this.girdawarCircleRepository;
            }
        }

        public GenericRepository<EFMoza> MozaRepository
        {
            get
            {
                if (this.mozaRepository == null)
                    this.mozaRepository = new GenericRepository<EFMoza>(context);
                return this.mozaRepository;
            }
        }

        public GenericRepository<EFGirdawar> GirdawarRepository
        {
            get
            {
                if (this.girdawarRepository == null)
                    this.girdawarRepository = new GenericRepository<EFGirdawar>(context);
                return girdawarRepository;
            }
        }

        public GenericRepository<EFCircleGirdawari> CircleGirdawariRepository
        {
            get
            {
                if (this.circleGirdawariRepository == null)
                    this.circleGirdawariRepository = new GenericRepository<EFCircleGirdawari>(context);
                return this.circleGirdawariRepository;
            }
        }

        public GenericRepository<EFPatwari> PatwariRepository
        {
            get
            {
                if (this.patwariRepository == null)
                    this.patwariRepository = new GenericRepository<EFPatwari>(context);
                return this.patwariRepository;
            }
        }

        public GenericRepository<EFMozaPatwari> MozaPatwariRepository
        {
            get
            {
                if (this.mozaPatwariRepository == null)
                    this.mozaPatwariRepository = new GenericRepository<EFMozaPatwari>(context);
                return this.mozaPatwariRepository;
            }
        }

        public GenericRepository<EFSettlementOfficer> SettlementOfficerRepository
        {
            get
            {
                if (this.settlementOfficerRepository == null)
                    this.settlementOfficerRepository = new GenericRepository<EFSettlementOfficer>(context);
                return this.settlementOfficerRepository;
            }
        }

        public GenericRepository<EFMonthlyProgress> ProgressRepository
        {
            get
            {
                if (this.progressRepository == null)
                    this.progressRepository = new GenericRepository<EFMonthlyProgress>(context);
                return this.progressRepository;
            }
        }

        public GenericRepository<EFUser> UserRepository
        {
            get
            {
                if (this.userRepository == null)
                    this.userRepository = new GenericRepository<EFUser>(context);
                return this.userRepository;
            }
        }

        public GenericRepository<EFRole> RoleRepository
        {
            get
            {
                if (this.roleRepository == null)
                    this.roleRepository = new GenericRepository<EFRole>(context);
                return this.roleRepository;
            }
        }

        public GenericRepository<EFUserRoles> UserRoleRepository
        {
            get
            {
                if (this.userRolesRepository == null)
                    this.userRolesRepository = new GenericRepository<EFUserRoles>(context);
                return this.userRolesRepository;
            }
        }

        public GenericRepository<EFSOVisits> SOVisitsRepository
        {
            get
            {
                if (this.soVisitsRepository == null)
                    this.soVisitsRepository = new GenericRepository<EFSOVisits>(context);
                return this.soVisitsRepository;
            }
        }

        public GenericRepository<EFTehsildarVisits> TehsildarVisitsRepository
        {
            get
            {
                if (this.tehsildarVisitsRepository == null)
                    this.tehsildarVisitsRepository = new GenericRepository<EFTehsildarVisits>(context);
                return this.tehsildarVisitsRepository;
            }
        }

        public GenericRepository<EFGirdawarVisits> GirdawarVisitsRepository
        {
            get
            {
                if (this.girdawarVisitsRepository == null)
                    this.girdawarVisitsRepository = new GenericRepository<EFGirdawarVisits>(context);
                return this.girdawarVisitsRepository;
            }
        }

        public GenericRepository<EFTehsilDar> TehsilDarRepository
        {
            get
            {
                if (this.tehsilDarRepository == null)
                    this.tehsilDarRepository = new GenericRepository<EFTehsilDar>(context);
                return this.tehsilDarRepository;
            }
        }

        public GenericRepository<EFMonitoringVisits> UserVisitsRepository
        {
            get
            {
                if (this.userVisitsRepository == null)
                    this.userVisitsRepository = new GenericRepository<EFMonitoringVisits>(context);
                return this.userVisitsRepository;
            }
        }



        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                List<string> lines = new List<string>();
                foreach(var error in e.Data)
                {
                    lines.Add(error.ToString());
                }
                System.IO.File.AppendAllLines(@"E:\errors.txt", lines);
                throw e;
            }
        }

        /// <summary>
        /// Save Async
        /// </summary>
        public async Task<int> SaveAsync()
        {
            try
            {
                return await Task<int>.Run(() => context.SaveChangesAsync());
            }
            catch (Exception e)
            {
                List<string> lines = new List<string>();
                foreach (var error in e.Data)
                {
                    lines.Add(error.ToString());
                }
                System.IO.File.AppendAllLines(@"E:\errors.txt", lines);
                throw e;
            }
        }

        private bool disposed = false;
        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
