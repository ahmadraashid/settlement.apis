﻿using Settlement.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Settlement.DAL
{
    public class SettlementDbContext : DbContext
    {
        public SettlementDbContext()
        {
        }

        public SettlementDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*var builder = new ConfigurationBuilder()
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            string connectionString = configuration.GetSection("Logging:ConnectionStrings:DefaultConnection").Value;
            optionsBuilder.UseSqlServer(connectionString);*/
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EFUser>()
                .HasIndex(u => u.UserName)
                .IsUnique();

            modelBuilder.Entity<EFRole>()
                .HasIndex(r => r.Role)
                .IsUnique();

            modelBuilder.Entity<EFUserRoles>()
                .HasKey(r => new { r.UserId, r.RoleId });
        }


        public DbSet<EFDistrict> Districts { get; set; }
        public DbSet<EFTehsil> Tehsils { get; set; }
        public DbSet<EFGirdawarCircle> GirdawarCircles { get; set; }
        public DbSet<EFTehsilDar> TehsilDars { get; set; }
        public DbSet<EFTehsilDari> TehsilDari { get; set; }
        public DbSet<EFGirdawar> Girdawars { get; set; }
        public DbSet<EFCircleGirdawari> CircleGirdawari { get; set; }
        public DbSet<EFMoza> Mozas { get; set; }
        public DbSet<EFPatwari> Patwaries { get; set; }
        public DbSet<EFMozaPatwari> MozaPatwari { get; set; }
        public DbSet<EFMonthlyProgress> MonthlyProgress { get; set; }
        public DbSet<EFSettlementOfficer> SettlementOfficers { get; set; }
        public DbSet<EFUser> Users { get; set; }
        public DbSet<EFRole> Roles { get; set; }
        public DbSet<EFUserRoles> UserRoles { get; set; }
        public DbSet<EFSOVisits> SOVisits { get; set; }
        public DbSet<EFTehsildarVisits> TehsilDarVisits { get; set; }
        public DbSet<EFGirdawarVisits> GirdawarVisits { get; set; }

        //Overridden SaveChanges to catch full exception details about
        //EntityValidation Exceptions instead of attaching debugger everytime
        public override int SaveChanges()
        {
            try
            {
                var entities = from e in ChangeTracker.Entries()
                               where e.State == EntityState.Added
                                   || e.State == EntityState.Modified
                               select e.Entity;
                foreach (var entity in entities)
                {
                    var validationContext = new ValidationContext(entity);
                    Validator.ValidateObject(entity, validationContext);
                }

                return base.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException(ex.Message, ex);
            }
        }

        private static readonly Dictionary<int, string> _sqlErrorTextDict = new Dictionary<int, string>
        {
                {547, "This operation failed because another data entry uses this entry."},
                {2601,"One of the properties is marked as Unique index and there is already an entry with that value."}
        };
    }
}
