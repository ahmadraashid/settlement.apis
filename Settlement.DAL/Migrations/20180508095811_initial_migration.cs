﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Settlement.DAL.Migrations
{
    public partial class initial_migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Girdawars",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Girdawars", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Patwaries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patwaries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SettlementOfficers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateEntered = table.Column<DateTime>(nullable: false),
                    DateLeft = table.Column<DateTime>(nullable: false),
                    DistrictId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SettlementOfficers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SettlementOfficers_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tehsils",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DistrictId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tehsils", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tehsils_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DistrictId = table.Column<int>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Districts_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "Districts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GirdawarCircles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    TehsilId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GirdawarCircles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GirdawarCircles_Tehsils_TehsilId",
                        column: x => x.TehsilId,
                        principalTable: "Tehsils",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EFUserId = table.Column<int>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Roles_Users_EFUserId",
                        column: x => x.EFUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CircleGirdawari",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateEntered = table.Column<DateTime>(nullable: false),
                    DateLeft = table.Column<DateTime>(nullable: false),
                    GirdawarCircleId = table.Column<int>(nullable: false),
                    GirdawarId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CircleGirdawari", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CircleGirdawari_GirdawarCircles_GirdawarCircleId",
                        column: x => x.GirdawarCircleId,
                        principalTable: "GirdawarCircles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CircleGirdawari_Girdawars_GirdawarId",
                        column: x => x.GirdawarId,
                        principalTable: "Girdawars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Mozas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FieldBooksVerified = table.Column<int>(nullable: false),
                    GirdawarCircleId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    PreMeasurements = table.Column<int>(nullable: false),
                    PreWrittenKhatoonis = table.Column<int>(nullable: false),
                    TotalKhasraJaat = table.Column<int>(nullable: false),
                    TotalKhatas = table.Column<int>(nullable: false),
                    TotalKhatoonis = table.Column<int>(nullable: false),
                    TotalMutations = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mozas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mozas_GirdawarCircles_GirdawarCircleId",
                        column: x => x.GirdawarCircleId,
                        principalTable: "GirdawarCircles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MonthlyProgress",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AksPatangi = table.Column<int>(nullable: false),
                    Dated = table.Column<DateTime>(nullable: false),
                    FieldBook = table.Column<int>(nullable: false),
                    KhassraMeasurement = table.Column<int>(nullable: false),
                    Mausaavis = table.Column<int>(nullable: false),
                    MisleHaqiyat = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    MozaId = table.Column<int>(nullable: false),
                    SOId = table.Column<int>(nullable: true),
                    ShajraKishtwaar = table.Column<int>(nullable: false),
                    ShajraNasbWriting = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MonthlyProgress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MonthlyProgress_Mozas_MozaId",
                        column: x => x.MozaId,
                        principalTable: "Mozas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MonthlyProgress_SettlementOfficers_SOId",
                        column: x => x.SOId,
                        principalTable: "SettlementOfficers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MozaPatwari",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateEntered = table.Column<DateTime>(nullable: false),
                    DateLeft = table.Column<DateTime>(nullable: false),
                    MozaId = table.Column<int>(nullable: false),
                    PatwariId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MozaPatwari", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MozaPatwari_Mozas_MozaId",
                        column: x => x.MozaId,
                        principalTable: "Mozas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MozaPatwari_Patwaries_PatwariId",
                        column: x => x.PatwariId,
                        principalTable: "Patwaries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CircleGirdawari_GirdawarCircleId",
                table: "CircleGirdawari",
                column: "GirdawarCircleId");

            migrationBuilder.CreateIndex(
                name: "IX_CircleGirdawari_GirdawarId",
                table: "CircleGirdawari",
                column: "GirdawarId");

            migrationBuilder.CreateIndex(
                name: "IX_GirdawarCircles_TehsilId",
                table: "GirdawarCircles",
                column: "TehsilId");

            migrationBuilder.CreateIndex(
                name: "IX_MonthlyProgress_MozaId",
                table: "MonthlyProgress",
                column: "MozaId");

            migrationBuilder.CreateIndex(
                name: "IX_MonthlyProgress_SOId",
                table: "MonthlyProgress",
                column: "SOId");

            migrationBuilder.CreateIndex(
                name: "IX_MozaPatwari_MozaId",
                table: "MozaPatwari",
                column: "MozaId");

            migrationBuilder.CreateIndex(
                name: "IX_MozaPatwari_PatwariId",
                table: "MozaPatwari",
                column: "PatwariId");

            migrationBuilder.CreateIndex(
                name: "IX_Mozas_GirdawarCircleId",
                table: "Mozas",
                column: "GirdawarCircleId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_EFUserId",
                table: "Roles",
                column: "EFUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Roles_Role",
                table: "Roles",
                column: "Role",
                unique: true,
                filter: "[Role] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_SettlementOfficers_DistrictId",
                table: "SettlementOfficers",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Tehsils_DistrictId",
                table: "Tehsils",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DistrictId",
                table: "Users",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserName",
                table: "Users",
                column: "UserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CircleGirdawari");

            migrationBuilder.DropTable(
                name: "MonthlyProgress");

            migrationBuilder.DropTable(
                name: "MozaPatwari");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Girdawars");

            migrationBuilder.DropTable(
                name: "SettlementOfficers");

            migrationBuilder.DropTable(
                name: "Mozas");

            migrationBuilder.DropTable(
                name: "Patwaries");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "GirdawarCircles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Tehsils");

            migrationBuilder.DropTable(
                name: "Districts");
        }
    }
}
