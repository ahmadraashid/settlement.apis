﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Settlement.DAL.Migrations
{
    public partial class AddedmoreattributestoSO : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "SettlementOfficers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DistrictId",
                table: "MonthlyProgress",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "SettlementOfficers");

            migrationBuilder.DropColumn(
                name: "DistrictId",
                table: "MonthlyProgress");
        }
    }
}
