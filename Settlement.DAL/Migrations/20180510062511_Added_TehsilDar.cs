﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Settlement.DAL.Migrations
{
    public partial class Added_TehsilDar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TehsilDars",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TehsilDars", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TehsilDari",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateEntered = table.Column<DateTime>(nullable: false),
                    DateLeft = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TehsilDarId = table.Column<int>(nullable: false),
                    TehsilId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TehsilDari", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TehsilDari_TehsilDars_TehsilDarId",
                        column: x => x.TehsilDarId,
                        principalTable: "TehsilDars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TehsilDari_TehsilDarId",
                table: "TehsilDari",
                column: "TehsilDarId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TehsilDari");

            migrationBuilder.DropTable(
                name: "TehsilDars");
        }
    }
}
