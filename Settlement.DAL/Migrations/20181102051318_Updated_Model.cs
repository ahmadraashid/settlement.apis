﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Settlement.DAL.Migrations
{
    public partial class Updated_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GirdawarVisits_MonthlyProgress_ProgressId",
                table: "GirdawarVisits");

            migrationBuilder.DropForeignKey(
                name: "FK_SOVisits_MonthlyProgress_ProgressId",
                table: "SOVisits");

            migrationBuilder.DropForeignKey(
                name: "FK_TehsilDarVisits_MonthlyProgress_ProgressId",
                table: "TehsilDarVisits");

            migrationBuilder.DropIndex(
                name: "IX_TehsilDarVisits_ProgressId",
                table: "TehsilDarVisits");

            migrationBuilder.DropIndex(
                name: "IX_SOVisits_ProgressId",
                table: "SOVisits");

            migrationBuilder.DropIndex(
                name: "IX_GirdawarVisits_ProgressId",
                table: "GirdawarVisits");

            migrationBuilder.DropColumn(
                name: "ProgressId",
                table: "TehsilDarVisits");

            migrationBuilder.DropColumn(
                name: "ProgressId",
                table: "SOVisits");

            migrationBuilder.DropColumn(
                name: "ProgressId",
                table: "GirdawarVisits");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "MonthlyProgress",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "MonthlyProgress");

            migrationBuilder.AddColumn<int>(
                name: "ProgressId",
                table: "TehsilDarVisits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgressId",
                table: "SOVisits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgressId",
                table: "GirdawarVisits",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TehsilDarVisits_ProgressId",
                table: "TehsilDarVisits",
                column: "ProgressId");

            migrationBuilder.CreateIndex(
                name: "IX_SOVisits_ProgressId",
                table: "SOVisits",
                column: "ProgressId");

            migrationBuilder.CreateIndex(
                name: "IX_GirdawarVisits_ProgressId",
                table: "GirdawarVisits",
                column: "ProgressId");

            migrationBuilder.AddForeignKey(
                name: "FK_GirdawarVisits_MonthlyProgress_ProgressId",
                table: "GirdawarVisits",
                column: "ProgressId",
                principalTable: "MonthlyProgress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SOVisits_MonthlyProgress_ProgressId",
                table: "SOVisits",
                column: "ProgressId",
                principalTable: "MonthlyProgress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TehsilDarVisits_MonthlyProgress_ProgressId",
                table: "TehsilDarVisits",
                column: "ProgressId",
                principalTable: "MonthlyProgress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
