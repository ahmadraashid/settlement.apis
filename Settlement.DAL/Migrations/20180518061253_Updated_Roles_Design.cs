﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Settlement.DAL.Migrations
{
    public partial class Updated_Roles_Design : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Roles_Users_EFUserId",
                table: "Roles");

            migrationBuilder.DropIndex(
                name: "IX_Roles_EFUserId",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "EFUserId",
                table: "Roles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EFUserId",
                table: "Roles",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Roles_EFUserId",
                table: "Roles",
                column: "EFUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Roles_Users_EFUserId",
                table: "Roles",
                column: "EFUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
