﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Settlement.DAL.Migrations
{
    public partial class Added_Monitoring_Visits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GirdawarVisits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Dated = table.Column<DateTime>(nullable: false),
                    GirdawarId = table.Column<int>(nullable: true),
                    IsOkay = table.Column<bool>(nullable: false),
                    MozaId = table.Column<int>(nullable: true),
                    ProgressId = table.Column<int>(nullable: true),
                    Remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GirdawarVisits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GirdawarVisits_Girdawars_GirdawarId",
                        column: x => x.GirdawarId,
                        principalTable: "Girdawars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GirdawarVisits_Mozas_MozaId",
                        column: x => x.MozaId,
                        principalTable: "Mozas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GirdawarVisits_MonthlyProgress_ProgressId",
                        column: x => x.ProgressId,
                        principalTable: "MonthlyProgress",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SOVisits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Dated = table.Column<DateTime>(nullable: false),
                    IsOkay = table.Column<bool>(nullable: false),
                    MozaId = table.Column<int>(nullable: true),
                    ProgressId = table.Column<int>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    SOId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SOVisits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SOVisits_Mozas_MozaId",
                        column: x => x.MozaId,
                        principalTable: "Mozas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SOVisits_MonthlyProgress_ProgressId",
                        column: x => x.ProgressId,
                        principalTable: "MonthlyProgress",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SOVisits_SettlementOfficers_SOId",
                        column: x => x.SOId,
                        principalTable: "SettlementOfficers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TehsilDarVisits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Dated = table.Column<DateTime>(nullable: false),
                    IsOkay = table.Column<bool>(nullable: false),
                    MozaId = table.Column<int>(nullable: true),
                    ProgressId = table.Column<int>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    TehsilDarId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TehsilDarVisits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TehsilDarVisits_Mozas_MozaId",
                        column: x => x.MozaId,
                        principalTable: "Mozas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TehsilDarVisits_MonthlyProgress_ProgressId",
                        column: x => x.ProgressId,
                        principalTable: "MonthlyProgress",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TehsilDarVisits_TehsilDars_TehsilDarId",
                        column: x => x.TehsilDarId,
                        principalTable: "TehsilDars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GirdawarVisits_GirdawarId",
                table: "GirdawarVisits",
                column: "GirdawarId");

            migrationBuilder.CreateIndex(
                name: "IX_GirdawarVisits_MozaId",
                table: "GirdawarVisits",
                column: "MozaId");

            migrationBuilder.CreateIndex(
                name: "IX_GirdawarVisits_ProgressId",
                table: "GirdawarVisits",
                column: "ProgressId");

            migrationBuilder.CreateIndex(
                name: "IX_SOVisits_MozaId",
                table: "SOVisits",
                column: "MozaId");

            migrationBuilder.CreateIndex(
                name: "IX_SOVisits_ProgressId",
                table: "SOVisits",
                column: "ProgressId");

            migrationBuilder.CreateIndex(
                name: "IX_SOVisits_SOId",
                table: "SOVisits",
                column: "SOId");

            migrationBuilder.CreateIndex(
                name: "IX_TehsilDarVisits_MozaId",
                table: "TehsilDarVisits",
                column: "MozaId");

            migrationBuilder.CreateIndex(
                name: "IX_TehsilDarVisits_ProgressId",
                table: "TehsilDarVisits",
                column: "ProgressId");

            migrationBuilder.CreateIndex(
                name: "IX_TehsilDarVisits_TehsilDarId",
                table: "TehsilDarVisits",
                column: "TehsilDarId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GirdawarVisits");

            migrationBuilder.DropTable(
                name: "SOVisits");

            migrationBuilder.DropTable(
                name: "TehsilDarVisits");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Users");
        }
    }
}
