﻿using AutoMapper;
using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IMozaService
    {
        /// <summary>
        /// Gets list of mozas for the provided girdawar circle id
        /// </summary>
        /// <param name="girdawarCircleId"></param>
        /// <returns></returns>
        IEnumerable<Moza> GetAll(int girdawarCircleId);

        /// <summary>
        /// Get user mozas
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<Moza> GetUserMozas(string userId);
    }

    public class MozaService : IMozaService
    {
        SettlementDbContext context;
        IMapper mapper;

        public MozaService(SettlementDbContext cntxt, IMapper autoMapper)
        {
            context = cntxt;
            mapper = autoMapper;
        }

        public IEnumerable<Moza> GetAll(int girdawarCircleId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var mozas = unitWork.MozaRepository.GetWithInclude(m => m.GirdawarCircle.Id.Equals(girdawarCircleId), new string[] { "GirdawarCircle" });
                return mapper.Map<List<Moza>>(mozas);
            }
        }

        public IEnumerable<Moza> GetUserMozas(string userId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<Moza> mozasList = new List<Moza>();
                var districts = unitWork.UserRepository.GetWithInclude(u => u.UserName.Equals(userId), new string[] { "District" });
                if (districts != null)
                {
                    int districtId = 0;
                    foreach(var district in districts)
                    {
                        districtId = district.District.Id;
                    }

                    var mozas = unitWork.MozaRepository.GetWithInclude(m => m.GirdawarCircle.Tehsil.District.Id.Equals(districtId), new string[] { "GirdawarCircle.Tehsil.District" });
                    mozasList = mapper.Map<List<Moza>>(mozas);
                }
                return mozasList;
                
            }
        }
    }
}
