﻿using AutoMapper;
using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IGirdawarCircleService
    {
        /// <summary>
        /// Gets list of girdawar circles for the provided tehsil circle
        /// </summary>
        /// <param name="tehsilId"></param>
        /// <returns></returns>
        IEnumerable<GirdawarCircle> GetAll(int tehsilId);
    }

    public class GirdawarCircleService : IGirdawarCircleService
    {
        SettlementDbContext context;
        IMapper mapper;

        public GirdawarCircleService(SettlementDbContext cntxt, IMapper autoMapper)
        {
            context = cntxt;
            mapper = autoMapper;
        }

        public IEnumerable<GirdawarCircle> GetAll(int tehsilId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var girdawarCircles = unitWork.GirdawarCircleRepository.GetWithInclude(g => g.Tehsil.Id.Equals(tehsilId), new string[] { "Tehsil" });
                return mapper.Map<List<GirdawarCircle>>(girdawarCircles);
            }
        }
    }
}
