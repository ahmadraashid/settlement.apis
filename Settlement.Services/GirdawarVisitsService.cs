﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IGirdawarVisitsService
    {
        /// <summary>
        /// Gets list of visits for the specified period
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        IEnumerable<MonitoringVisits> Get(DateTime startDate, DateTime dateEnd);

        /// <summary>
        /// Adds a new monitoring visit
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        ActionResponse Add(NewMonitoringVisit visit);
    }

    public class GirdawarVisitsService
    {
        SettlementDbContext context;

        public GirdawarVisitsService(SettlementDbContext cntxt)
        {
            this.context = cntxt;
        }

        public IEnumerable<MonitoringVisits> Get(DateTime startDate, DateTime endDate)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<MonitoringVisits> visitsList = new List<MonitoringVisits>();
                var visits = unitWork.GirdawarVisitsRepository.GetWithInclude(v => v.Dated >= startDate &&
                    v.Dated <= endDate, new string[] { "Moza", "MonthlyProgress" });

                foreach (var visit in visits)
                {
                    visitsList.Add(new MonitoringVisits()
                    {
                        Id = visit.Id,
                        Moza = visit.Moza.Name,
                        Remarks = visit.Remarks,
                        IsOkay = visit.IsOkay,
                        Dated = visit.Dated.ToLongDateString()
                    });
                }
                return visitsList;
            }
        }

        public ActionResponse Add(NewMonitoringVisit visit)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { ReturnedId = 0, Message = null, Success = true };
                IMessageHelper mHelper = new MessageHelper();

                var moza = unitWork.MozaRepository.GetByID(visit.MozaId);
                if (moza == null)
                {
                    response.Success = false;
                    response.Message = mHelper.GetNotFound("Moza");
                    return response;
                }

                var girdawar = unitWork.GirdawarRepository.GetByID(visit.VisitorId);
                if (girdawar == null)
                {
                    response.Success = false;
                    response.Message = mHelper.GetNotFound("Girdawar");
                    return response;
                }

                unitWork.GirdawarVisitsRepository.Insert(new EFGirdawarVisits()
                {
                    Moza = moza,
                    Girdawar = girdawar,
                    Remarks = visit.Remarks,
                    IsOkay = visit.IsOkay,
                    Dated = visit.Dated
                });

                unitWork.Save();
                return response;
            }
        }
    }
}
