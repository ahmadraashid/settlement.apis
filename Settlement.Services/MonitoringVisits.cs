﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IUserMonitoringVisits
    {
        /// <summary>
        /// Gets list of visits for the specified period
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        IEnumerable<MonitoringVisits> Get(DateTime startDate, DateTime dateEnd);

        /// <summary>
        /// Adds a new monitoring visit
        /// </summary>
        /// <param name="visit"></param>
        /// <returns></returns>
        ActionResponse AddSOVisit(NewMonitoringVisit visit);
    }

    public class UserMonitoringVisits
    {
        SettlementDbContext context;

        public UserMonitoringVisits(SettlementDbContext cntxt)
        {
            this.context = cntxt;
        }

        public IEnumerable<MonitoringVisits> Get(DateTime startDate, DateTime endDate)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<MonitoringVisits> visitsList = new List<MonitoringVisits>();
                var visits = unitWork.UserVisitsRepository.GetWithInclude(v => v.Dated >= startDate &&
                    v.Dated <= endDate, new string[] { "Moza", "MonthlyProgress" });

                foreach (var visit in visits)
                {
                    visitsList.Add(new MonitoringVisits()
                    {
                        Id = visit.Id,
                        Moza = visit.Moza.Name,
                        Remarks = visit.Remarks,
                        IsOkay = visit.IsOkay,
                        Dated = visit.Dated.ToLongDateString()
                    });
                }
                return visitsList;
            }
        }

        public ActionResponse AddSOVisit(NewMonitoringVisit visit)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { ReturnedId = 0, Message = null, Success = true };
                IMessageHelper mHelper = new MessageHelper();

                var moza = unitWork.MozaRepository.GetByID(visit.MozaId);
                if (moza == null)
                {
                    response.Success = false;
                    response.Message = mHelper.GetNotFound("Moza");
                    return response;
                }

                var so = unitWork.UserRepository.Get(u => u.UserName.Equals(visit.VisitorId));
                if (so == null)
                {
                    response.Success = false;
                    response.Message = mHelper.GetNotFound("User");
                    return response;
                }

                var role = unitWork.RoleRepository.Get(r => r.Role.Equals("SO"));
                if (role == null)
                {
                    response.Success = false;
                    response.Message = mHelper.GetNotFound("SO Role");
                    return response;
                }

                unitWork.UserVisitsRepository.Insert(new EFMonitoringVisits()
                {
                    Moza = moza,
                    User = so,
                    Role = role,
                    Remarks = visit.Remarks,
                    IsOkay = visit.IsOkay,
                    Dated = visit.Dated
                });

                unitWork.Save();
                return response;
            }
        }
    }
}
