﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IPatwariService
    {
        /// <summary>
        /// Gets list of patwaris for the provided girdawar circle
        /// </summary>
        /// <param name="girdawarCircleId"></param>
        /// <returns></returns>
        IEnumerable<PatwariView> GetInGirdawarCircle(int girdawarCircleId);

        /// <summary>
        /// Adds new patwari with the provided information
        /// </summary>
        /// <param name="patwari"></param>
        /// <returns></returns>
        ActionResponse Add(NewPatwari patwari);

        /// <summary>
        /// Assigns patwari to specific moza
        /// </summary>
        /// <param name="assignParams"></param>
        /// <returns></returns>
        ActionResponse AssignPatwariToMoza(AssignPatwari assignParams);

        /// <summary>
        /// Releases patwari from a Moza
        /// </summary>
        /// <param name="releaseParams"></param>
        /// <returns></returns>
        ActionResponse RealesePatwariFromMoza(ReleasePatwari releaseParams);
    }

    public class PatwariService : IPatwariService
    {
        SettlementDbContext context;
        IMessageHelper msgHelper;
        ActionResponse response;

        public PatwariService(SettlementDbContext cntxt)
        {
            context = cntxt;
            msgHelper = new MessageHelper();
        }

        public IEnumerable<PatwariView> GetInGirdawarCircle(int girdawarCircleId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var patwaris = unitWork.MozaPatwariRepository
                    .GetWithInclude(p => p.Status == CurrentStatus.Active && p.Moza.GirdawarCircle.Id.Equals(girdawarCircleId), new string[] { "Moza", "Moza.GirdawarCircle", "Patwari" });

                List<PatwariView> patwarisList = new List<PatwariView>();
                foreach (var patwari in patwaris)
                {
                    if (patwari.Patwari != null)
                    {
                        patwarisList.Add(new PatwariView()
                        {
                            Id = patwari.Patwari.Id,
                            Name = patwari.Patwari.Name,
                            Moza = patwari.Moza.Name,
                            SinceDated = patwari.DateEntered
                        });
                    }
                }
                return patwarisList;
            }
        }

        public ActionResponse Add(NewPatwari patwari)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                response = new ActionResponse() { Success = true, ReturnedId = 0, Message = "" };
                var moza = unitWork.MozaRepository.GetByID(patwari.MozaId);
                if (moza == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Moza");
                    return response;
                }

                var newPatwari = unitWork.PatwariRepository.Insert(new EFPatwari() { Name = patwari.Name });
                var newGirdawri = unitWork.MozaPatwariRepository.Insert(new EFMozaPatwari()
                {
                    Patwari = newPatwari,
                    Moza = moza,
                    DateEntered = DateTime.Now,
                    Status = CurrentStatus.Active
                });
                response.ReturnedId = newGirdawri.Id;
                unitWork.Save();
                return response;
            }
        }

        public ActionResponse AssignPatwariToMoza(AssignPatwari assignParams)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                response = new ActionResponse() { Success = true, ReturnedId = 0, Message = "" };
                var patwari = unitWork.PatwariRepository.GetByID(assignParams.PatwariId);
                if (patwari == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Patwari");
                    return response;
                }

                var moza = unitWork.MozaRepository.GetByID(assignParams.MozaId);
                if (moza == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Moza");
                    return response;
                }

                unitWork.MozaPatwariRepository.Insert(new EFMozaPatwari()
                {
                    Moza = moza,
                    Patwari = patwari,
                    Status = CurrentStatus.Active,
                    DateEntered = DateTime.Now
                });

                unitWork.Save();
                return response;
            }
        }

        public ActionResponse RealesePatwariFromMoza(ReleasePatwari releaseParams)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                response = new ActionResponse() { Success = true, ReturnedId = 0, Message = "" };
                var patwari = unitWork.PatwariRepository.GetByID(releaseParams.PatwariId);
                if (patwari == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Patwari");
                    return response;
                }

                var moza = unitWork.MozaRepository.GetByID(releaseParams.MozaId);
                if (moza == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Moza");
                    return response;
                }

                var mozaPatwari = unitWork.MozaPatwariRepository
                    .GetOne(p => p.PatwariId.Equals(releaseParams.PatwariId) &&
                    p.MozaId.Equals(releaseParams.MozaId));

                if (mozaPatwari == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Moza and Patwari");
                    return response;
                }

                mozaPatwari.Status = CurrentStatus.NotActive;
                mozaPatwari.DateLeft = DateTime.Now;
                unitWork.MozaPatwariRepository.Update(mozaPatwari);

                unitWork.Save();
                return response;
            }
        }
    }
}
