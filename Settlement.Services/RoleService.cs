﻿using Settlement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IRoleService
    {
        /// <summary>
        /// Gets list of all roles
        /// </summary>
        /// <returns></returns>
        IEnumerable<Role> Get();
    }

    public class RoleService
    {
    }
}
