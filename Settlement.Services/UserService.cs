﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Settlement.Services
{
    public interface IUserService
    {
        /// <summary>
        /// Gets list of all users
        /// </summary>
        /// <returns></returns>
        List<UserView> Get();

        /// <summary>
        /// Gets list of users for the provided district
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        List<UserView> GetInDistrict(int districtId);

        /// <summary>
        /// Gets list of SOS for the provided districts
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        IEnumerable<UserList> GetSosForDistrict(int districtId);

        /// <summary>
        /// Authenticate user with the provided username
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        UserView AuthenticateUser(string userName, string password);

        /// <summary>
        /// Adds a new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        ActionResponse Add(NewUser user);

        /// <summary>
        /// Manage roles for a user
        /// </summary>
        /// <param name="manageRoles"></param>
        /// <returns></returns>
        ActionResponse ManageAssignRoles(ManageRoles manageRoles);
    }

    public class UserService : IUserService
    {
        SettlementDbContext context;
        IMessageHelper msgHelper;

        public UserService(SettlementDbContext cntxt)
        {
            context = cntxt;
        }

        public UserView AuthenticateUser(string userName, string password)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                UserView foundUser = new UserView();
                var findUser = unitWork.UserRepository.GetWithInclude(u => u.UserName.Equals(userName) && u.Password.Equals(password),
                    new string[] { "Roles.Role", "District" });

                if (findUser != null)
                {
                    foreach(var user in findUser)
                    {
                        foundUser.UserName = user.UserName;
                        foundUser.FullName = user.FullName;
                        foundUser.DistrictId = user.District.Id;
                        foundUser.District = user.District.Name;
                        List<string> roles = new List<string>();
                        foreach(var role in user.Roles)
                        {
                            roles.Add(role.Role.Role);
                        }
                        foundUser.Roles = roles;
                        break;
                    }
                }
                return foundUser;
            }
        }

        public List<UserView> Get()
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<UserView> usersList = new List<UserView>();
                var users = unitWork.UserRepository.GetWithInclude(u => u.Id != 0, new string[] { "District", "Roles.Role" });

                foreach(var user in users)
                {
                    List<string> userRoles = new List<string>();
                    if (user.Roles.Count > 0)
                    {
                        foreach(var role in user.Roles)
                        {
                            userRoles.Add(role.Role.Role);
                        }
                    }

                    usersList.Add(new UserView()
                    {
                        UserName = user.UserName,
                        District= user.District.Name,
                        Roles = userRoles
                    });
                }
                return usersList;
            }
        }

        public IEnumerable<UserList> GetSosForDistrict(int districtId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<UserList> usersList = new List<UserList>();
                var users = unitWork.UserRepository.GetWithInclude(u => u.District.Id.Equals(districtId), new string[] { "District" });
                if (users != null)
                {
                    foreach (var user in users)
                    {
                        usersList.Add(new UserList()
                        {
                            UserId = user.Id,
                            FullName = user.FullName
                        });
                    }
                }
                return usersList;
            }
        }

        public List<UserView> GetInDistrict(int districtId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<UserView> usersList = new List<UserView>();
                var users = unitWork.UserRepository.GetWithInclude(u => u.District.Id == districtId, new string[] { "District", "Roles.Role" });

                foreach (var user in users)
                {
                    List<string> userRoles = new List<string>();
                    if (user.Roles.Count > 0)
                    {
                        foreach (var role in user.Roles)
                        {
                            userRoles.Add(role.Role.Role);
                        }
                    }

                    usersList.Add(new UserView()
                    {
                        UserName = user.UserName,
                        District = user.District.Name,
                        Roles = userRoles
                    });
                }
                return usersList;
            }
        }

        public ActionResponse ManageAssignRoles(ManageRoles manageRoles)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = false, Message = "", ReturnedId = 0 };
                var user = unitWork.UserRepository.Get(u => u.UserName.Equals(manageRoles.UserName));

                if (user == null)
                {
                    response.Message = msgHelper.GetNotFound("User");
                    return response;
                }

                if (manageRoles.Roles.Count > 0)
                {
                    foreach(var role in manageRoles.Roles)
                    {
                        var roleObj = unitWork.RoleRepository.Get(r => r.Role.Equals(role));
                        if (roleObj == null)
                        {
                            response.Message = msgHelper.GetNotFound("Role");
                            return response;
                        }

                        unitWork.UserRoleRepository.Insert(new EFUserRoles()
                        {
                            User = user,
                            Role = roleObj
                        });
                    }

                    unitWork.Save();
                }

                return response;
            }
        }

        public ActionResponse Add(NewUser user)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = false, Message = "", ReturnedId = 0 };
                var district = unitWork.DistrictRepository.GetByID(user.DistrictId);
                msgHelper = new MessageHelper();

                if (district == null)
                {
                    response.Message = msgHelper.GetNotFound("District");
                    return response;
                }

                var userNameExists = unitWork.UserRepository.Get(u => u.UserName.Equals(user.UserName));
                if (userNameExists != null)
                {
                    response.Message = msgHelper.UserNameAlreadyTaken(user.UserName);
                    return response;
                }

                using (var scope = new TransactionScope())
                {
                    var newUser = unitWork.UserRepository.Insert(new EFUser()
                    {
                        UserName = user.UserName,
                        District = district,
                        Password = user.Password,
                        Status = CurrentStatus.Active
                    });

                    unitWork.Save();
                    if (user.Roles.Count > 0)
                    {
                        foreach(var role in user.Roles)
                        {
                            var roleObj = unitWork.RoleRepository.Get(r => r.Role.Equals(role));
                            if (roleObj == null)
                            {
                                response.Message = msgHelper.GetNotFound("Role");
                                return response;
                            }

                            unitWork.UserRoleRepository.Insert(new EFUserRoles()
                            {
                                User = newUser,
                                Role = roleObj
                            });
                        }
                        unitWork.Save();
                    }

                    scope.Complete();
                    response.Message = msgHelper.NewRecord("User");
                }
                return response;
            }
        }
    }
}
