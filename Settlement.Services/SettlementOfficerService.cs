﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface ISettlementOfficerService
    {
        /// <summary>
        /// Gets list of settlement officer
        /// </summary>
        /// <returns></returns>
        List<SettlementOfficer> GetAll();

        /// <summary>
        /// assign settlement officer to a district
        /// </summary>
        /// <param name="assignParams"></param>
        /// <returns></returns>
        //ActionResponse AssignSettlementOfficer(AssignSO assignParams);

        /// <summary>
        /// Adds new settlement officer
        /// </summary>
        /// <param name="sOfficer"></param>
        /// <returns></returns>
        ActionResponse Add(NewSettlementOfficer sOfficer);
    }

    public class SettlementOfficerService : ISettlementOfficerService
    {
        SettlementDbContext context;
        IMessageHelper msgHelper;
        ActionResponse response;

        public SettlementOfficerService(SettlementDbContext cntxt)
        {
            context = cntxt;
            msgHelper = new MessageHelper();
        }

        public List<SettlementOfficer> GetAll()
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var sOfficers = unitWork.SettlementOfficerRepository.GetAll();
                List<SettlementOfficer> sOfficersList = new List<SettlementOfficer>();
                foreach(var sOfficer in sOfficers)
                {
                    sOfficersList.Add(new SettlementOfficer()
                    {
                        Id = sOfficer.Id
                    });
                }
                return sOfficersList;
            }
        }

        public ActionResponse Add(NewSettlementOfficer newSo)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                response = new ActionResponse() { Success = true, Message = "", ReturnedId = 0 };
                try
                {
                    var district = unitWork.DistrictRepository.GetByID(newSo.DistrictId);
                    if (district == null)
                    {
                        response.Success = false;
                        response.Message = msgHelper.GetNotFound("District");
                        return response;
                    }

                    //TODO
                    //Deactivate SO's old assignment
                    unitWork.SettlementOfficerRepository.Insert(new EFSettlementOfficer()
                    {
                        Name = newSo.Name,
                        District = district,
                        DateEntered = DateTime.Now,
                        Status = CurrentStatus.Active
                    });

                    unitWork.Save();
                    response.ReturnedId = 1;
                }
                catch(Exception ex)
                {
                    response.Success = false;
                    response.Message = ex.Message;
                }
            }
            return response;
        }
        
    }
}
