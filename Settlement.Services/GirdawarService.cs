﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IGirdawarService
    {
        /// <summary>
        /// Gets list of girdawars for the provided tehsil
        /// </summary>
        /// <param name="tehsilId"></param>
        /// <returns></returns>
        IEnumerable<GirdawarView> GetInTehsil(int tehsilId);

        /// <summary>
        /// Adds new girdawar with the provided information
        /// </summary>
        /// <param name="girdawar"></param>
        /// <returns></returns>
        ActionResponse Add(NewGirdawar girdawar);

        /// <summary>
        /// Assigns girdawar to specific circle
        /// </summary>
        /// <param name="assignParams"></param>
        /// <returns></returns>
        ActionResponse AssignGirdawarToCircle(AssignGirdawar assignParams);

        /// <summary>
        /// Releases girdawar from a circle
        /// </summary>
        /// <param name="releaseParams"></param>
        /// <returns></returns>
        ActionResponse RealeseGirdawarFromCircle(ReleaseGirdawar releaseParams);
    }

    public class GirdawarService : IGirdawarService
    {
        SettlementDbContext context;
        IMessageHelper msgHelper;
        ActionResponse response;

        public GirdawarService(SettlementDbContext cntxt)
        {
            context = cntxt;
            msgHelper = new MessageHelper();
        }

        public IEnumerable<GirdawarView> GetInTehsil(int tehsilId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var girdawars = unitWork.CircleGirdawariRepository
                    .GetWithInclude(c => c.Status == CurrentStatus.Active && c.GirdawarCircle.Tehsil.Id.Equals(tehsilId), new string[] { "GirdawarCircle", "Girdawar" } );

                List<GirdawarView> girdawarsList = new List<GirdawarView>();
                foreach(var girdawar in girdawars)
                {
                    if (girdawar.Girdawar != null)
                    {
                        girdawarsList.Add(new GirdawarView()
                        {
                            Id = girdawar.Girdawar.Id,
                            Name = girdawar.Girdawar.Name,
                            GirdawarCircle = girdawar.GirdawarCircle.Name,
                            SinceDated = girdawar.DateEntered
                        });
                    }
                }
                return girdawarsList;
            }
        }

        public ActionResponse Add(NewGirdawar girdawar)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = true, ReturnedId = 0, Message = "" };
                //using (var transaction = context.Database.BeginTransaction())
                //{
                    var girdawarCircle = unitWork.GirdawarCircleRepository.GetByID(girdawar.GirdawarCircleId);
                    if (girdawarCircle == null)
                    {
                        response.Success = false;
                        response.Message = msgHelper.GetNotFound("Girdawar Circle");
                        return response;
                    }

                    var newGirdawar = unitWork.GirdawarRepository.Insert(new EFGirdawar() { Name = girdawar.Name });
                    var newGirdawri = unitWork.CircleGirdawariRepository.Insert(new EFCircleGirdawari()
                    {
                        Girdawar = newGirdawar,
                        GirdawarCircle = girdawarCircle,
                        DateEntered = DateTime.Now,
                        Status = CurrentStatus.Active
                    });
                    response.ReturnedId = newGirdawri.Id;
                //transaction.Commit();
                //}
                unitWork.Save();
                return response;
            }
        }

        public ActionResponse AssignGirdawarToCircle(AssignGirdawar assignParams)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                response = new ActionResponse() { Success = true, ReturnedId = 0, Message = "" };
                var girdawar = unitWork.GirdawarRepository.GetByID(assignParams.GirdawarId);
                if (girdawar == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Girdawar");
                    return response;
                }

                var circle = unitWork.GirdawarCircleRepository.GetByID(assignParams.CircleId);
                if (circle == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Circle");
                    return response;
                }

                unitWork.CircleGirdawariRepository.Insert(new EFCircleGirdawari()
                {
                    GirdawarCircle = circle,
                    Girdawar = girdawar,
                    Status = CurrentStatus.Active,
                    DateEntered = DateTime.Now
                });

                unitWork.Save();
                return response;
            }
        }

        public ActionResponse RealeseGirdawarFromCircle(ReleaseGirdawar releaseParams)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                response = new ActionResponse() { Success = true, ReturnedId = 0, Message = "" };
                var girdawar = unitWork.GirdawarRepository.GetByID(releaseParams.GirdawarId);
                if (girdawar == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Girdawar");
                    return response;
                }

                var circle = unitWork.GirdawarCircleRepository.GetByID(releaseParams.CircleId);
                if (circle == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Circle");
                    return response;
                }

                var circleGirdawar = unitWork.CircleGirdawariRepository
                    .GetOne(p => p.GirdawarId.Equals(releaseParams.GirdawarId) &&
                    p.GirdawarCircleId.Equals(releaseParams.CircleId));

                if (circleGirdawar == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Circle and Girdawar");
                    return response;
                }

                circleGirdawar.Status = CurrentStatus.NotActive;
                circleGirdawar.DateLeft = DateTime.Now;
                unitWork.CircleGirdawariRepository.Update(circleGirdawar);

                unitWork.Save();
                return response;
            }
        }
    }
}
