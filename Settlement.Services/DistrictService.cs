﻿using AutoMapper;
using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using System;
using System.Collections.Generic;

namespace Settlement.Services
{
    public interface IDistrictService
    {
        /// <summary>
        /// Gets all districts
        /// </summary>
        /// <returns></returns>
        IEnumerable<District> GetAll();
    }

    public class DistrictService : IDistrictService
    {
        SettlementDbContext context;
        IMapper mapper;

        public DistrictService(SettlementDbContext cntxt, IMapper autoMapper)
        {
            context = cntxt;
            mapper = autoMapper;
        }

        public IEnumerable<District> GetAll()
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var districts = unitWork.DistrictRepository.GetAll();
                return mapper.Map<List<District>>(districts);
            }
        }
    }
}
