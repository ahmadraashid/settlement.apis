﻿using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using Settlement.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface IProgressService
    {
        /// <summary>
        /// Gets the monthly progress for the provided id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        MonthlyProgressView Get(int id);


        /// <summary>
        /// Get all progress reports for the provided Month and Year
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        IEnumerable<MonthlyProgressView> GetAll(int month, int year);

        /// <summary>
        /// Gets unapproved Summaries for the given district
        /// </summary>
        /// <returns></returns>
        IEnumerable<MonthlyProgressView> GetUnApproved(int districtId);

        /// <summary>
        /// Gets Rejected Summaries for the given district
        /// </summary>
        /// <returns></returns>
        IEnumerable<MonthlyProgressView> GetRejected(int districtId);

        /// <summary>
        /// Approves provided summaries
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        ActionResponse ApproveSummaryProgress(UpdateProgressStatus progress);

        /// <summary>
        /// Set provided summaries as Rejected
        /// </summary>
        /// <param name="progress"></param>
        /// <returns></returns>
        ActionResponse RejectSummaryProgress(UpdateProgressStatus progress);

        /// <summary>
        /// Adds new progress report with provided parameters
        /// </summary>
        /// <param name="newProgress"></param>
        /// <returns></returns>
        ActionResponse Add(MonthlyProgress newProgress);

        /// <summary>
        /// Updates the progress with the provided object
        /// </summary>
        /// <param name="updateProgress"></param>
        /// <returns></returns>
        ActionResponse Update(UpdateMonthlyProgress updateProgress);
    }

    public class ProgressService : IProgressService
    {
        SettlementDbContext context;
        IMessageHelper msgHelper;
        public ProgressService(SettlementDbContext cntxt)
        {
            context = cntxt;
        }

        public MonthlyProgressView Get(int id)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                MonthlyProgressView pView = new MonthlyProgressView();
                var monthlyProgress = unitWork.ProgressRepository.GetWithInclude(p => p.Id.Equals(id), new string[] { "Moza", "SO" });
                foreach (var progress in monthlyProgress)
                {
                    string description = "";
                    if (!string.IsNullOrEmpty(progress.Description))
                        description = progress.Description;

                    pView.Id = progress.Id;
                    pView.Month = progress.Month;
                    pView.Year = progress.Year;
                    pView.MozaId = progress.Moza.Id;
                    pView.AksPatangi = progress.AksPatangi;
                    pView.KhatooniWriting = progress.KhatooniWriting;
                    pView.ShajraNasbWriting = progress.ShajraNasbWriting;
                    pView.ShajraKishtwaar = progress.ShajraKishtwaar;
                    pView.KhassraMeasurement = progress.KhassraMeasurement;
                    pView.FieldBook = progress.FieldBook;
                    pView.MisleHaqiyat = progress.MisleHaqiyat;
                    pView.Mausaavis = progress.Mausaavis;
                    pView.SettlementOfficer = progress.SO.Name;
                    pView.IsApproved = progress.Status;
                    pView.Description = progress.Description;
                }
                return pView;
            }
        }

        public IEnumerable<MonthlyProgressView> GetAll(int month, int year)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<MonthlyProgressView> progressList = new List<MonthlyProgressView>();
                var monthlyProgress = unitWork.ProgressRepository.GetWithInclude(p => p.Year.Equals(year) && p.Month.Equals(month), new string[] { "Moza", "SO" });
                foreach (var progress in monthlyProgress)
                {
                    progressList.Add(new MonthlyProgressView()
                    {
                        Month = progress.Month,
                        Year = progress.Year,
                        Moza = progress.Moza.Name,
                        AksPatangi = progress.AksPatangi,
                        ShajraNasbWriting = progress.ShajraNasbWriting,
                        ShajraKishtwaar = progress.ShajraKishtwaar,
                        KhassraMeasurement = progress.KhassraMeasurement,
                        FieldBook = progress.FieldBook,
                        MisleHaqiyat = progress.MisleHaqiyat,
                        Mausaavis = progress.Mausaavis,
                        SettlementOfficer = progress.SO.Name,
                        IsApproved = progress.Status,
                        Description = progress.Description
                    });
                }
                return progressList;
            }
        }

        public ActionResponse ApproveSummaryProgress(UpdateProgressStatus progress)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = true, Message = "", ReturnedId = 0 };
                var so = unitWork.SettlementOfficerRepository.Get(s => s.DistrictId.Equals(progress.DistrictId));
                if (so == null)
                {
                    response.Message = msgHelper.GetInvalidAttempt("Settlement Officer");
                    response.Success = false;
                    return response;
                }

                var summaries = unitWork.ProgressRepository.GetWithInclude(p => progress.Ids.Contains(p.Id) && p.DistrictId == progress.DistrictId, new string[] { "Moza", "SO" });
                foreach (var summary in summaries)
                {
                    summary.Status = ApprovalStatus.Approved;
                }

                unitWork.Save();
                return response;
            }
        }

        public ActionResponse RejectSummaryProgress(UpdateProgressStatus progress)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = true, Message = "", ReturnedId = 0 };
                var so = unitWork.SettlementOfficerRepository.Get(s => s.DistrictId.Equals(progress.DistrictId));
                if (so == null)
                {
                    response.Message = msgHelper.GetInvalidAttempt("Settlement Officer");
                    response.Success = false;
                    return response;
                }

                var summaries = unitWork.ProgressRepository.GetWithInclude(p => progress.Ids.Contains(p.Id) && p.DistrictId == progress.DistrictId, new string[] { "Moza", "SO" });
                foreach (var summary in summaries)
                {
                    summary.Status = ApprovalStatus.Rejected;
                }

                unitWork.Save();
                return response;
            }
        }

        public IEnumerable<MonthlyProgressView> GetUnApproved(int districtId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<MonthlyProgressView> progressList = new List<MonthlyProgressView>();
                var monthlyProgress = unitWork.ProgressRepository.GetWithInclude(p => p.Status == ApprovalStatus.NotApproved && p.DistrictId == districtId, new string[] { "Moza", "SO" });
                foreach (var progress in monthlyProgress)
                {
                    progressList.Add(new MonthlyProgressView()
                    {
                        Id = progress.Id,
                        Month = progress.Month,
                        Year = progress.Year,
                        Moza = progress.Moza.Name,
                        KhatooniWriting = progress.KhatooniWriting,
                        AksPatangi = progress.AksPatangi,
                        ShajraNasbWriting = progress.ShajraNasbWriting,
                        ShajraKishtwaar = progress.ShajraKishtwaar,
                        KhassraMeasurement = progress.KhassraMeasurement,
                        FieldBook = progress.FieldBook,
                        MisleHaqiyat = progress.MisleHaqiyat,
                        Mausaavis = progress.Mausaavis,
                        SettlementOfficer = progress.SO.Name,
                        IsApproved = progress.Status,
                        Description = progress.Description
                    });
                }
                return progressList;
            }
        }

        public IEnumerable<MonthlyProgressView> GetRejected(int districtId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                List<MonthlyProgressView> progressList = new List<MonthlyProgressView>();
                var monthlyProgress = unitWork.ProgressRepository.GetWithInclude(p => p.Status == ApprovalStatus.Rejected && p.DistrictId == districtId, new string[] { "Moza", "SO" });
                foreach (var progress in monthlyProgress)
                {
                    progressList.Add(new MonthlyProgressView()
                    {
                        Id = progress.Id,
                        Month = progress.Month,
                        Year = progress.Year,
                        Moza = progress.Moza.Name,
                        KhatooniWriting = progress.KhatooniWriting,
                        AksPatangi = progress.AksPatangi,
                        ShajraNasbWriting = progress.ShajraNasbWriting,
                        ShajraKishtwaar = progress.ShajraKishtwaar,
                        KhassraMeasurement = progress.KhassraMeasurement,
                        FieldBook = progress.FieldBook,
                        MisleHaqiyat = progress.MisleHaqiyat,
                        Mausaavis = progress.Mausaavis,
                        SettlementOfficer = progress.SO.Name,
                        IsApproved = progress.Status,
                        Description = progress.Description
                    });
                }
                return progressList;
            }
        }


        public ActionResponse Add(MonthlyProgress newProgress)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = true, Message = "", ReturnedId = 0 };
                var moza = unitWork.MozaRepository.GetByID(newProgress.MozaId);
                msgHelper = new MessageHelper();

                if (moza == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Moza");
                    return response;
                }

                var so = unitWork.SettlementOfficerRepository.Get(s => s.DistrictId.Equals(newProgress.SOId));
                int districtId = newProgress.SOId;
                if (so == null)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFound("Settlement Officer");
                    return response;
                }

                var progressObj = unitWork.ProgressRepository.GetWithInclude(
                    p => p.Month.Equals(newProgress.Month) && p.Year.Equals(newProgress.Year) &&
                    p.Moza.Id.Equals(newProgress.MozaId), new string[] { "Moza" });


                var progressExist = progressObj.GetEnumerator().Current;
                if (progressExist != null)
                {
                    progressExist.ShajraNasbWriting = newProgress.ShajraNasbWriting;
                    progressExist.ShajraKishtwaar = newProgress.ShajraKishtwaar;
                    progressExist.KhassraMeasurement = newProgress.KhassraMeasurement;
                    progressExist.Mausaavis = newProgress.Mausaavis;
                    progressExist.MisleHaqiyat = newProgress.MisleHaqiyat;
                    progressExist.Dated = DateTime.Now;
                }
                else
                {
                    unitWork.ProgressRepository.Insert(new EFMonthlyProgress()
                    {
                        DistrictId = districtId,
                        Moza = moza,
                        SO = so,
                        Year = newProgress.Year,
                        Month = newProgress.Month,
                        KhatooniWriting = newProgress.KhatooniWriting,
                        AksPatangi = newProgress.AksPatangi,
                        FieldBook = newProgress.FieldBook,
                        ShajraNasbWriting = newProgress.ShajraNasbWriting,
                        ShajraKishtwaar = newProgress.ShajraKishtwaar,
                        KhassraMeasurement = newProgress.KhassraMeasurement,
                        Mausaavis = newProgress.Mausaavis,
                        MisleHaqiyat = newProgress.MisleHaqiyat,
                        Dated = DateTime.Now,
                        Status = ApprovalStatus.NotApproved,
                        Description = newProgress.Description
                    });
                }

                unitWork.Save();
                return response;
            }
        }

        public ActionResponse Update(UpdateMonthlyProgress updateProgress)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                ActionResponse response = new ActionResponse() { Success = false, Message = "", ReturnedId = 0 };
                int progressId = updateProgress.Id;
                if (progressId == 0)
                {
                    response.Message = msgHelper.GetNotFound("Monthly Progress");
                    return response;
                }

                var moza = unitWork.MozaRepository.GetByID(updateProgress.MozaId);
                if (moza == null)
                {
                    response.Message = msgHelper.GetNotFound("Moza");
                    return response;
                }

                var monthlyProgress = unitWork.ProgressRepository.GetWithInclude(p => p.Id.Equals(progressId), new string[] { "Moza", "SO" });
                
                foreach (var progress in monthlyProgress)
                {
                    progress.Id = updateProgress.Id;
                    progress.Month = updateProgress.Month;
                    progress.Year = updateProgress.Year;
                    progress.Moza = moza;
                    progress.KhatooniWriting = updateProgress.KhatooniWriting;
                    progress.AksPatangi = updateProgress.AksPatangi;
                    progress.ShajraNasbWriting = updateProgress.ShajraNasbWriting;
                    progress.ShajraKishtwaar = updateProgress.ShajraKishtwaar;
                    progress.KhassraMeasurement = updateProgress.KhassraMeasurement;
                    progress.FieldBook = updateProgress.FieldBook;
                    progress.MisleHaqiyat = updateProgress.MisleHaqiyat;
                    progress.Mausaavis = updateProgress.Mausaavis;
                    progress.Status = ApprovalStatus.NotApproved;
                    progress.Description = updateProgress.Description;
                }

                try
                {
                    unitWork.Save();
                    response.Success = true;
                    response.ReturnedId = updateProgress.Id;
                }
                catch(Exception ex)
                {
                    response.Message = ex.Message;
                }
                return response;
            }
        }
    }
}
