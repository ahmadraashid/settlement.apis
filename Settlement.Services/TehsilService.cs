﻿using AutoMapper;
using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Settlement.Services
{
    public interface ITehsilService
    {
        /// <summary>
        /// Gets list of tehsils for the provided districts
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        IEnumerable<Tehsil> GetAll(int districtId);
    }

    public class TehsilService : ITehsilService
    {
        SettlementDbContext context;
        IMapper mapper;

        public TehsilService(SettlementDbContext cntxt, IMapper autoMapper)
        {
            context = cntxt;
            mapper = autoMapper;
        }

        public IEnumerable<Tehsil> GetAll(int districtId)
        {
            using (var unitWork = new UnitOfWork(context))
            {
                var tehsils = unitWork.TehsilRepository.GetWithInclude(t => t.District.Id.Equals(districtId), new string[] { "District" });
                return mapper.Map<List<Tehsil>>(tehsils);
            }
        }
    }
}
