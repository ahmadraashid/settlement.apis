﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Settlement.APIs.AutoMapper;
using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace Settlement.APIs
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /*services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
           .AddJwtBearer(options =>
           {
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = true,
                   //ValidIssuer = "yourdomain.com",
                   //ValidAudience = "yourdomain.com",
                   IssuerSigningKey = new SymmetricSecurityKey(
                       Encoding.UTF8.GetBytes(Configuration["JwtKey"]))
               };
           });*/
            services.AddCors();
            string connectionString = Configuration.GetSection("Logging:ConnectionStrings:DefaultConnection").Value;
            services.AddDbContext<SettlementDbContext>(
                options =>
                {
                    options.UseSqlServer(connectionString,
                    sqlOptions => sqlOptions.EnableRetryOnFailure());
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Settlement APIs, PMU BOR, Govt of KP",
                    Description = "APIs for Settlement, PMU BOR, Govt of KP",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "Rashid Ahmad", Email = "raashid.ahmad@gmail.com", Url = "www.google.com.pk" }
                });
            });

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<SettlementDbContext>()
                .AddDefaultTokenProviders();


                // ===== Add Jwt Authentication ========
                //JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "PMU",
                    ValidAudience = "PMU",
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["JwtKey"]))
                };
            });



            services.AddAutoMapper(a => a.AddProfile(new MappingProfile()));
            services.AddScoped<IDistrictService, DistrictService>();
            services.AddScoped<ITehsilService, TehsilService>();
            services.AddScoped<IGirdawarService, GirdawarService>();
            services.AddScoped<IMozaService, MozaService>();
            services.AddScoped<IPatwariService, PatwariService>();
            services.AddScoped<ISettlementOfficerService, SettlementOfficerService>();
            services.AddScoped<IProgressService, ProgressService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISOVisitsService, SOVisitsService>();
            
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
             );

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Settlement APIs Version 1, PMU BOR, Govt of KP");
            });

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
