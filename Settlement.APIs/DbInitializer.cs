﻿using Microsoft.EntityFrameworkCore;
using Settlement.DAL;
using Settlement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Settlement.APIs
{
    public class DbInitializer
    {
        public SettlementDbContext context;

        public DbInitializer()
        {

        }

        public void SetDbContext(SettlementDbContext ctx)
        {
            context = ctx;
        }

        public void Seed()
        {
            // Run Migrations
            context.Database.Migrate();

            if (!context.Districts.Any())
            {
                // Seeding the Database

                //Set identities to start from 0
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Districts', RESEED, 1)");
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Tehsils', RESEED, 1)");
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('GirdawarCircles', RESEED, 1)");
                context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Mozas', RESEED, 1)");
                context.SaveChanges();

                //Districts
                var districtMansehra = context.Districts.Add(new EFDistrict(){ Name = "Mansehra" });

                //Tehsils
                var baffaTehsil = context.Tehsils.Add(new EFTehsil() { Name = "Baffa", District = districtMansehra.Entity });
                var bherKund = context.Tehsils.Add(new EFTehsil() { Name = "BherKund", District = districtMansehra.Entity });

                //Girdawar Circles
                var baffa1Circle = context.GirdawarCircles.Add(new EFGirdawarCircle() { Name = "Baffa 1", Tehsil = baffaTehsil.Entity });
                var baffa2Circle = context.GirdawarCircles.Add(new EFGirdawarCircle() { Name = "Baffa 2", Tehsil = baffaTehsil.Entity });
                var baffa3Circle = context.GirdawarCircles.Add(new EFGirdawarCircle() { Name = "Baffa 3", Tehsil = baffaTehsil.Entity });

                //Mozas
                var baffaBajoriKKhel = context.Mozas.Add(new EFMoza() { Name = "Baffa Bajori K Khel", GirdawarCircle = baffa1Circle.Entity });
                var baffaLughmaniLyariTetwal = context.Mozas.Add(new EFMoza() { Name = "Baffa Lughmani Lyari Tetwal", GirdawarCircle = baffa1Circle.Entity });

                //Add Girdawars
                var tanveerShahzadGirdawar = context.Girdawars.Add(new EFGirdawar() { Name = "Tanveer Shahzaad" });
                context.CircleGirdawari.Add(
                    new EFCircleGirdawari()
                    {
                        DateEntered = DateTime.Now,
                        Girdawar = tanveerShahzadGirdawar.Entity,
                        GirdawarCircle = baffa1Circle.Entity,
                        Status = CurrentStatus.Active
                    });

                var adminRole = context.Roles.Add(new EFRole() { Role = "Admin" });
                var soRole = context.Roles.Add(new EFRole() { Role = "SO" });
                var operatorRole = context.Roles.Add(new EFRole() { Role = "Operator" });
                var managerRole = context.Roles.Add(new EFRole() { Role = "Manager" });

                var adminUser = context.Users.Add(new EFUser()
                {
                    UserName = "admin",
                    FullName = "Administrator",
                    Password = "8622f0f69c91819119a8acf60a248d7b36fdb7ccf857ba8f85cf7f2767ff8265",
                    District = districtMansehra.Entity,
                    Status = CurrentStatus.Active
                });

                var soUser = context.Users.Add(new EFUser()
                {
                    UserName = "so",
                    FullName = "SO Mansehra",
                    Password = "8622f0f69c91819119a8acf60a248d7b36fdb7ccf857ba8f85cf7f2767ff8265",
                    District = districtMansehra.Entity,
                    Status = CurrentStatus.Active
                });

                var operatorUser = context.Users.Add(new EFUser()
                {
                    UserName = "operator",
                    FullName = "CO Mansehra",
                    Password = "8622f0f69c91819119a8acf60a248d7b36fdb7ccf857ba8f85cf7f2767ff8265",
                    District = districtMansehra.Entity,
                    Status = CurrentStatus.Active
                });

                var so = context.SettlementOfficers.Add(new EFSettlementOfficer()
                {
                    Name = "SO Mansehra",
                    District = districtMansehra.Entity,
                    Status = CurrentStatus.Active,
                    DateEntered = DateTime.Now,
                });

                var adminRoles = context.UserRoles.Add(new EFUserRoles()
                {
                    User = adminUser.Entity,
                    Role = adminRole.Entity
                });

                var soRoles = context.UserRoles.Add(new EFUserRoles()
                {
                    User = soUser.Entity,
                    Role = soRole.Entity
                });

                var operatorRoles = context.UserRoles.Add(new EFUserRoles()
                {
                    User = operatorUser.Entity,
                    Role = operatorRole.Entity
                });
                //adminUser.Entity.Roles.Add();
                //soUser.Entity.Roles.Add(soRole.Entity);
                //operatorUser.Entity.Roles.Add(operatorRole.Entity);

                context.SaveChanges();
            }
        }
    }
}
