﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Progress")]
    public class ProgressController : Controller
    {
        IProgressService progressService;

        public ProgressController(IProgressService pService)
        {
            progressService = pService;
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            if (id == 0)
            {
                return BadRequest("Id cannot be zero");
            }
            var progressView = progressService.Get(id);
            return Ok(progressView);
        }

        [HttpGet("{year:int}/{month:int}")]
        public IActionResult Get(int year, int month)
        {
            var progresses = progressService.GetAll(month, year);
            return Ok(progresses);
        }

        [HttpGet]
        [Route("GetUnApproved")]
        public IActionResult GetUnApprovedSummaries()
        {
            string districtStr = User.FindFirst(ClaimTypes.Country)?.Value;
            int districtId = 0;
            int.TryParse(districtStr, out districtId);
            var summaries = progressService.GetUnApproved(districtId);
            return Ok(summaries);
        }

        [HttpGet]
        [Route("GetRejected")]
        public IActionResult GetRejectedSummaries()
        {
            string districtStr = User.FindFirst(ClaimTypes.Country)?.Value;
            int districtId = 0;
            int.TryParse(districtStr, out districtId);
            var summaries = progressService.GetRejected(districtId);
            return Ok(summaries);
        }

        [HttpPost]
        [Route("Approve")]
        [Authorize(Roles = "Admin, SO")]
        public IActionResult ApproveSummaries([FromBody] UpdateProgressStatus progress)
        {
            string districtStr = User.FindFirst(ClaimTypes.Country)?.Value;
            int districtId = 0;
            int.TryParse(districtStr, out districtId);
            progress.DistrictId = districtId;
            var response = progressService.ApproveSummaryProgress(progress);
            return Ok();
        }

        [HttpPost]
        [Route("Reject")]
        [Authorize(Roles = "Admin, SO")]
        public IActionResult RejectSummaries([FromBody] UpdateProgressStatus progress)
        {
            string districtStr = User.FindFirst(ClaimTypes.Country)?.Value;
            int districtId = 0;
            int.TryParse(districtStr, out districtId);
            progress.DistrictId = districtId;
            var response = progressService.RejectSummaryProgress(progress);
            return Ok();
        }

        [HttpPost]
        public IActionResult Add([FromBody] MonthlyProgress progress)
        {
            string districtStr = User.FindFirst(ClaimTypes.Country)?.Value;
            if (!string.IsNullOrEmpty(districtStr))
            {
                progress.SOId = Convert.ToInt32(districtStr);
            }

            var response = progressService.Add(progress);
            if (response.Success)
            {
                return Ok(response.ReturnedId);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] UpdateMonthlyProgress progress)
        {
            var response = progressService.Update(progress);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response.Message);
        }
    }
}