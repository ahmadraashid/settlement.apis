﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        IUserService userService;

        public UserController(IUserService service)
        {
            userService = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var users = userService.Get();
            return Ok(users);
        }

        [HttpGet("{districtId:int}", Name = "GetInDistrict")]
        public IActionResult GetInDistrict(int districtId)
        {
            var users = userService.GetInDistrict(districtId);
            return Ok(users);
        }

        [HttpGet]
        [Route("GetSoS")]
        public IActionResult GetSoS()
        {
            int districtId = 0;
            var districtStr = HttpContext.User.FindFirst(ClaimTypes.Country)?.Value;
            if (!string.IsNullOrEmpty(districtStr))
            {
                districtId = Convert.ToInt32(districtStr);
            }
            else
            {
                return BadRequest("User data is invalid");
            }
            var sosList = userService.GetSosForDistrict(districtId);
            return Ok();
        }

        [HttpPost]
        public IActionResult Add([FromBody] NewUser user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = userService.Add(user);
            if (response.Success)
            {
                return Ok(response.ReturnedId);
            }
            return BadRequest(response.Message);
        }

        [HttpPost]
        [Route("manageUserRoles")]
        public IActionResult ManageUserRoles([FromBody] ManageRoles manageRoles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = userService.ManageAssignRoles(manageRoles);
            if (response.Success)
            {
                return Ok(response.ReturnedId);
            }

            return BadRequest(response.Message);
        }
    }
}