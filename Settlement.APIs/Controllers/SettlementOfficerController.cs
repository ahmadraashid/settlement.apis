﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/SettlementOfficer")]
    public class SettlementOfficerController : Controller
    {
        ISettlementOfficerService soService;

        public SettlementOfficerController(ISettlementOfficerService service)
        {
            soService = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var sos = soService.GetAll();
            return Ok(sos);
        }

        [HttpPost]
        public IActionResult Add(NewSettlementOfficer newSo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = soService.Add(newSo);
            if (response.Success)
            {
                return Ok(response.ReturnedId);
            }
            return BadRequest(response.Message);
        }
    }
}