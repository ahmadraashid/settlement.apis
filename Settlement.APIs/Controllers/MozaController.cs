﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Moza")]
    public class MozaController : Controller
    {
        IMozaService mozaService;

        public MozaController(IMozaService service)
        {
            mozaService = service;
        }

        [HttpGet("{tehsilCircleId:int}")]
        public IActionResult GetTehsilMozas(int tehsilCircleId)
        {
            var mozas = mozaService.GetAll(tehsilCircleId);
            return Ok(mozas);
        }

        [HttpGet]
        public IActionResult GetMozasForUser()
        {
            string userId = User.FindFirst(ClaimTypes.Name)?.Value;
            if (userId == null)
            {
                return Unauthorized();
            }
            var mozas = mozaService.GetUserMozas(userId);
            return Ok(mozas);
        }
    }
}