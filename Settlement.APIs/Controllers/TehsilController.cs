﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/Tehsil")]
    public class TehsilController : Controller
    {
        ITehsilService tehsilService;

        public TehsilController(ITehsilService service)
        {
            tehsilService = service;
        }

        [HttpGet("{districtId:int}", Name = "GetDistrictTehsils")]
        public IActionResult GetDistrictTehsil(int districtId)
        {
            var tehsils = tehsilService.GetAll(districtId);
            return Ok(tehsils);
        }
    }
}