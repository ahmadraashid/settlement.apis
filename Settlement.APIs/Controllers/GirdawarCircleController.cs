﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/GirdawarCircle")]
    public class GirdawarCircleController : Controller
    {
        IGirdawarCircleService girdawarCircleService;

        public GirdawarCircleController(IGirdawarCircleService service)
        {
            girdawarCircleService = service;
        }

        [HttpGet("{tehsilCircleId:int}", Name = "GetGirdawarCircles")]
        public IActionResult GetGirdawarCircles(int tehsilCircleId)
        {
            var girdawarCircles = girdawarCircleService.GetAll(tehsilCircleId);
            return Ok(girdawarCircles);
        }
    }
}