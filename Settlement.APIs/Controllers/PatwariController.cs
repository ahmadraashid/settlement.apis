﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/Patwari")]
    public class PatwariController : Controller
    {
        IPatwariService patwariService;

        public PatwariController(IPatwariService gService)
        {
            patwariService = gService;
        }

        [HttpGet("{girdawarCircleId:int}", Name = "GetInGirdawarCircle")]
        public IActionResult GetInGirdawarCircle(int girdawarCircleId)
        {
            var patwaris = patwariService.GetInGirdawarCircle(girdawarCircleId);
            return Ok(patwaris);
        }

        [HttpPost]
        public IActionResult Add(NewPatwari patwari)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = patwariService.Add(patwari);
            if (response.Success)
            {
                return Ok(response.ReturnedId);
            }
            return BadRequest(response.Message);
        }

        [HttpPost]
        [Route("AssignPatwariToMoza")]
        public IActionResult AssignPatwariToMoza([FromBody] AssignPatwari assignParams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = patwariService.AssignPatwariToMoza(assignParams);
            if (response.Success)
            {
                return Ok(response.Message);
            }
            return BadRequest(response.Message);
        }

        [HttpPost]
        [Route("ReleasePatwariFromMoza")]
        public IActionResult ReleasePatwariFromMoza([FromBody] ReleasePatwari releaseParams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = patwariService.RealesePatwariFromMoza(releaseParams);
            if (response.Success)
            {
                return Ok(response.Message);
            }
            return BadRequest(response.Message);
        }
    }
}