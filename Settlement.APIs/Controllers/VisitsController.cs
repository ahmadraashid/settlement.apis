﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/Visits")]
    public class VisitsController : Controller
    {
        ISOVisitsService soVisitsService;

        public VisitsController(ISOVisitsService service)
        {
            soVisitsService = service;
        }

        [HttpGet]
        public IActionResult Get(VisitsFilter filter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            bool isValidDate = false;

            isValidDate = DateTime.TryParse(filter.StartDate, out startDate);
            if (!isValidDate)
            {
                return BadRequest("Invalid start date provided");
            }
            isValidDate = DateTime.TryParse(filter.EndDate, out endDate);
            if (isValidDate)
            {
                return BadRequest("Invalid end date provided");
            }

            var soVisitss = soVisitsService.Get(startDate, endDate);
            return Ok(soVisitss);
        }

        [HttpPost]
        [Route("SOVisit")]
        public IActionResult SOVisit([FromBody] NewMonitoringVisit visit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //string roles = HttpContext.User.FindFirst(ClaimTypes.Role)?.Value;
            //string userId = HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;
            /*if (string.IsNullOrEmpty(userId))
            {
                return BadRequest("User is not authorized");
            }*/

            //visit.VisitorId = userId;
            var response = soVisitsService.Add(visit);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.ReturnedId);
        }
    }
}