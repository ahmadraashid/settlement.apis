﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Settlement.APIs.Utilities;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        IConfiguration configuration;
        IUserService userService;

        public AccountController(IConfiguration _configuration, IUserService _userService)
        {
            configuration = _configuration;
            userService = _userService;
        }

        [HttpPost]
        [Route("token")]
        public IActionResult Token([FromBody] AuthenticateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            SecurityHelper sHelper = new SecurityHelper();
            model.Password = sHelper.GetPasswordHash(model.Password);
            var foundUser = userService.AuthenticateUser(model.UserName, model.Password);

            if (!string.IsNullOrEmpty(foundUser.UserName))
            {
                TokenManager tManager = new TokenManager();
                string securityKey = configuration["JwtKey"];
                string rolesStr = String.Join(",", foundUser.Roles);
                string token = tManager.GenerateToken(securityKey, foundUser.UserName, rolesStr, foundUser.DistrictId.ToString());
                TokenView view = new TokenView() { Token = token, FullName = foundUser.FullName, Roles = foundUser.Roles, District = foundUser.District };
                return Ok(view);
            }
            return Unauthorized();
        }

        
    }
}