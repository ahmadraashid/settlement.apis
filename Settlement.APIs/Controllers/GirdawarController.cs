﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.Models;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/Girdawar")]
    public class GirdawarController : Controller
    {
        IGirdawarService girdawarService;

        public GirdawarController(IGirdawarService gService)
        {
            girdawarService = gService;
        }

        [HttpGet("{tehsilId:int}", Name = "GetGirdawarsInTehsil")]
        public IActionResult GetGirdawarsInTehsil(int tehsilId)
        {
            var girdawars = girdawarService.GetInTehsil(tehsilId);
            return Ok(girdawars);
        }

        [HttpPost]
        public IActionResult Add(NewGirdawar girdawar)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = girdawarService.Add(girdawar);
            if (response.Success)
            {
                return Ok(response.ReturnedId);
            }
            return BadRequest(response.Message);
        }

        [HttpPost]
        [Route("AssignGirdawarToCircle")]
        public IActionResult AssignGirdawarToCircle([FromBody] AssignGirdawar assignParams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = girdawarService.AssignGirdawarToCircle(assignParams);
            if (response.Success)
            {
                return Ok(response.Message);
            }
            return BadRequest(response.Message);
        }

        [HttpPost]
        [Route("ReleaseGirdawarFromCircle")]
        public IActionResult ReleaseGirdawarFromCircle([FromBody] ReleaseGirdawar releaseParams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = girdawarService.RealeseGirdawarFromCircle(releaseParams);
            if (response.Success)
            {
                return Ok(response.Message);
            }
            return BadRequest(response.Message);
        }
    }
}