﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Settlement.DAL;
using Settlement.DAL.UnitOfWork;
using Settlement.Services;

namespace Settlement.APIs.Controllers
{
    [Produces("application/json")]
    [Route("api/District")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DistrictController : Controller
    {
        IDistrictService districtService;

        public DistrictController(IDistrictService service)
        {
            districtService = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var districts = districtService.GetAll();
            return Ok(districts);
        }
    }
}